from django.contrib import admin

# Register your models here.

from .models import Wine, Grape

@admin.register(Wine)
class WineAdmin(admin.ModelAdmin):
    list_display = ('name', 'year', 'country', 'region', 'price', 'description')
    list_filter = ('year', 'country', 'region', 'price')
    search_fields = ('name', 'year', 'country', 'region', 'price', 'description')

@admin.register(Grape)
class GrapeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'best_of')
    list_filter = ('name', 'description', 'best_of')
    search_fields = ('name', 'description', 'best_of')
    