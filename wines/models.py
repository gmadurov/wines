from django.db import models

# Create your models here.

class Wine(models.Model):
    name = models.CharField(max_length=200)
    year = models.IntegerField()

    grape = models.ForeignKey("Grape", related_name='wines_list', verbose_name=("grape kind"), on_delete=models.CASCADE)

    country = models.CharField(max_length=200, blank=True, null=True)
    region = models.CharField(max_length=200, blank=True, null=True)
    label = models.ImageField(upload_to='labels')
    price = models.FloatField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.name} {self.year} ({self.grape})"
    

class Grape(models.Model):
    name = models.CharField(max_length=200)
    
    description = models.TextField()

    best_of = models.ForeignKey(Wine, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='best_of')

    def __str__(self):
        return self.name